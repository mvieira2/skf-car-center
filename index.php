<?php get_header(); 
global $wpdb;


$query_depoimentos =  "SELECT * FROM `{$wpdb->prefix}posts` WHERE `post_type` = 'depoimentos' AND `post_status` = 'publish' ORDER BY `post_date` ASC LIMIT 3";
$depoimentos = $wpdb->get_results( $wpdb->prepare($query_depoimentos, OBJECT ) );

// echo "<pre>";
// print_r( $query_depoimentos );
// print_r( $depoimentos );
// echo "</pre>";

?>


<section class="main">
	<div class="banner ">
		<?php 
		echo do_shortcode('[smartslider3 slider=1]');
		?>
	</div>

	<div class="box1 " data-animate-in="fadeIn">
		<div class="center text-center">
			<p>O SKF CAR CENTER é uma rede de oficinas mecânicas especializadas
				<br> na aplicação de produtos SKF. Ele foi criado para levar a confiança <br>
			e a qualidade da SKF aos proprietários e profissionais apaixonados por carros. </p>
			<br>
			<p>Aqui, você cuida do seu veículo com a garantia de nossas peças 
				<br> de reposição automotiva e conta com profissionais capacitados
				<br> para atender altos níveis de exigência.</p>
			</div>
		</div>
		<div class="beneficios" data-animate-in="fadeIn">
			<div class="center">
				<h2 class="text-center">Outros benefícios de ser um credenciado SKF Car Center:</h2>

				<div class="list-beneficios">
					<div>
						<p>Identidade visual própria</p>
					</div>
					<div>
						<p>Formação e assistência técnica contínua</p>
					</div>
					<div>
						<p>Maior capacidade de compra para os produtos SKF</p>
					</div>
					<div>
						<p>Diluição dos custos por atividade</p>
					</div>
					<div>
						<p>Credenciamento a uma rede forte e que fornece total apoio ao negócio</p>
					</div>
					<div>
						<p>Inteligência de negócios</p>
					</div>
					<div>
						<p>Consultoria e auxílio à gestão</p>
					</div>
					<div>
						<p>Indicadores de performance</p>
					</div>
				</div>
			</div>
		</div>

			<!-- 		
		        <div class="banner" data-animate-in="fadeIn">
					<div>
						<a href="">
							<img src="<?php echo get_template_directory_uri(); ?>/img/banner5.png">
						</a>
					</div>
				</div> 			
			-->

			<div class="box3 parallax" style='background-image: url("<?php echo get_template_directory_uri(); ?>/img/banner5.1-otimizado.jpg");'>
				<div class="text-middle">
					<div class="text-center ">
						<h2 class="font-circular">Seja um credenciado SKF Car Center</h2>
						<p>Ser um credenciado é contar com a tecnologia SKF Car Center, isso significa uma parceria de sucesso: sua oficina mecânica e a SKF, marca reconhecida mundialmente pela qualidade de seus produtos.</p>
					</div>
				</div>
			</div>

			<div class="depoimentos" data-animate-in="fadeIn">
				<div class="center">
					<h2 class="text-center">Confira os depoimentos dos credenciados:</h2>

					<div class="list-photos">
						<?php foreach ($depoimentos as $depoimento) { ?>

							<div>
								<a href="javascript:;" >
									<?php

									$urlImage = get_post_meta( $depoimento->ID, 'urlFileUnique', true );

									if ($urlImage) {
										echo '<img src="'.$urlImage.'">';
									}	
									else if (has_post_thumbnail($depoimento->ID)) {
										echo '<img src="'.get_the_post_thumbnail_url( $depoimento->ID,  array('200', '200') ).'">';
									}
									else {
										echo '<img src="'.get_template_directory_uri().'/img/not-image.png">';
									}
									?>

									<p class="fadeIn animated">
										<span><?php echo $depoimento->post_title ; ?></span> <br>
										<small><?php echo get_post_meta( $depoimento->ID, 'local', true ); ?></small>
									</p>
									<p class="hide">
										<?php echo $depoimento->post_excerpt ?  $depoimento->post_excerpt : $depoimento->post_content; ?>
									</p>
								</a>
							</div>
						<?php } ?>

					</div>

					<div class="comments animated fadeInUp">
						<p class="text-center"></p>
					</div>

				</div>
			</div>



			<div class="box2" data-animate-in="fadeIn">
				<div class="text-center">
					<p>
						<strong style="
						font-size: 36px;
						margin-bottom: -14px;
						display: block;
						">Ficou interessado?</strong>
						<br>
						Preencha o formulário de cadastro <br> e transforme o seu negócio em um SKF Car Center.

						<br>
						<br>

						<button class="botao1" onclick="document.getElementById('popup').style.display='block';">Junte-se a nós!</button>
					</p>
				</div>

			</div>

		</section>


		<?php get_footer(); ?>