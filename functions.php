<?php



// Add Thumbnail Theme Support
add_theme_support('post-thumbnails');
add_image_size('large', 700, '', true); // Large Thumbnail
add_image_size('medium', 250, '', true); // Medium Thumbnail
add_image_size('small', 120, '', true); // Small Thumbnail
add_image_size('custom-size', 700, 200, true); 

add_action('init', 'create_post_type');
function create_post_type()
{

    register_taxonomy_for_object_type('category', 'Depoimentos'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'Depoimentos');
    register_post_type('depoimentos', // Register Custom Post Type
      array(
        'labels' => array(
            'name' => __('Depoimentos', ''), // Rename these to suit
            'singular_name' => __('Depoimento', ''),
            'add_new' => __('Adicionar', ''),
            'add_new_item' => __('Adicionar Depoimento', ''),
            'edit' => __('Editar', ''),
            'edit_item' => __('Editar Depoimento', ''),
            'new_item' => __('Novo Depoimento', ''),
            'view' => __('Ver Depoimento', ''),
            'view_item' => __('Ver Depoimento', ''),
            'search_items' => __('Procurar Depoimento', ''),
            'not_found' => __('Nenhum Depoimento', ''),
            'not_found_in_trash' => __('Nenhum Depoimento encontrado na lixeira', '')
          ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'menu_icon'   => 'dashicons-format-status',
        'supports' => array(
          'title',
          'editor',
          'excerpt',
          'thumbnail'
        ), // Go to Dashboard Custom Depoimento post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
          'post_tag',
          'category'
        ) // Add Category and Post Tags support
      ));



    register_taxonomy_for_object_type('category', 'Oficinas'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'Oficinas');
    register_post_type('oficinas', // Register Custom Post Type
      array(
        'labels' => array(
            'name' => __('Oficinas', ''), // Rename these to suit
            'singular_name' => __('Oficina', ''),
            'add_new' => __('Adicionar', ''),
            'add_new_item' => __('Adicionar Oficina', ''),
            'edit' => __('Editar', ''),
            'edit_item' => __('Editar Oficina', ''),
            'new_item' => __('Novo Oficina', ''),
            'view' => __('Ver Oficina', ''),
            'view_item' => __('Ver Oficina', ''),
            'search_items' => __('Procurar Oficina', ''),
            'not_found' => __('Nenhum Oficina', ''),
            'not_found_in_trash' => __('Nenhum Oficina encontrado na lixeira', '')
          ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'menu_icon'   => 'dashicons-hammer',
        'supports' => array(
          'title',
          'editor',
          'excerpt',
          'thumbnail'
        ), // Go to Dashboard Custom Oficina post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
          'post_tag',
          'category'
        ) // Add Category and Post Tags support
      ));




  }

//Register Meta Box
  function rm_register_meta_box() {
    add_meta_box( 'rm-meta-box-id', esc_html__( 'Mais informações', 'text-domain' ), 'rm_meta_box_callback', 'depoimentos', 'advanced', 'high' );
    add_meta_box( 'rm-meta-box-id', esc_html__( 'Mais informações', 'text-domain' ), 'rm_meta_box_endereco_callback', 'oficinas', 'advanced', 'high' );

  }

  add_action( 'add_meta_boxes', 'rm_register_meta_box');

//Add field
  function rm_meta_box_endereco_callback( $meta_id ) {

    $outline = '<label for="endereco" style="width:150px; display:inline-block;">'. esc_html__('Endereço', 'text-domain') .'</label>';
    $endereco = get_post_meta( $meta_id->ID, 'endereco', true );
    $outline .= '<input type="text" name="endereco" id="endereco" class="endereco" value="'. esc_attr($endereco) .'" style="width:300px;"/>';


    echo $outline;
  }

//Add field
  function rm_meta_box_callback( $meta_id ) {

    $outline = '<label for="local" style="width:150px; display:inline-block;">'. esc_html__('Local', 'text-domain') .'</label>';
    $local = get_post_meta( $meta_id->ID, 'local', true );
    $outline .= '<input type="text" name="local" id="local" class="local" value="'. esc_attr($local) .'" style="width:300px;"/>';

    $outline .= '<br><br><label for="urlFileUnique" style="width:150px; display:inline-block;">'. esc_html__('URL Image', 'text-domain') .'</label>';
    $urlFileUnique = get_post_meta( $meta_id->ID, 'urlFileUnique', true );
    $outline .= '<input type="text" name="urlFileUnique" id="urlFileUnique" class="urlFileUnique" value="'. esc_attr($urlFileUnique) .'" style="width:300px;"/>';


    echo $outline;
  }




  add_action( 'save_post', 'myplugin_save_postdata' );

  /* Quando o post for salvo, salvamos também nossos dados personalizados */
  function myplugin_save_postdata( $post_id ) {

  // É necessário verificar se o usuário está autorizado a fazer isso
    if ( 'page' == $_POST['post_type'] ) {
      if ( ! current_user_can( 'edit_page', $post_id ) )
        return;
    } else {
      if ( ! current_user_can( 'edit_post', $post_id ) )
        return;
    }

  // Recebe o ID do post
    $post_ID = $_POST['post_ID'];

  // Remove caracteres indesejados
    $mydata = sanitize_text_field( $_POST['local'] );
    $urlFileUnique = sanitize_text_field( $_POST['urlFileUnique'] );
    $endereco = sanitize_text_field( $_POST['endereco'] );

  // Adicionamos ou atualizados o $mydata 
    update_post_meta($post_ID, 'local', $mydata);
    update_post_meta($post_ID, 'urlFileUnique', $urlFileUnique);
    update_post_meta($post_ID, 'endereco', $endereco);


  }



// [depoimentos limit="4"]
  function depoimentos( $atts ) {
    global $wpdb;
    $html="";
    $a = shortcode_atts( array(
      'limit' => '4',
    ), $atts );

    $query_depoimentos =  "SELECT * FROM `{$wpdb->prefix}posts` WHERE `post_type` = 'depoimentos' AND `post_status` = 'publish' ORDER BY `post_date` ASC LIMIT ". $a["limit"];
    $depoimentos = $wpdb->get_results( $wpdb->prepare($query_depoimentos, OBJECT ) );

    $html .= '<div class="depoimentos" data-animate-in="fadeIn">
    <div class="center">
    <div class="list-photos">';


    foreach ($depoimentos as $depoimento) {

      $html .= '<div><a href="javascript:;" >';


      if (has_post_thumbnail($depoimento->ID)) {
        $html .= '<img src="'.get_the_post_thumbnail_url( $depoimento->ID ).'">';
      }
      else {

        $html .= '<img src="'.get_template_directory_uri().'/img/not-image.png">';
      }


      $html .= '<p class="fadeIn animated">
      <span> '.$depoimento->post_title.' </span> <br>
      <small>'.get_post_meta( $depoimento->ID, "local", true ).'</small>
      </p>';

      $depo = $depoimento->post_excerpt ?  $depoimento->post_excerpt : $depoimento->post_content;

      $html .= '
      <p class="hide">'.$depo . '</p>
      </a>
      </div>';

    } 

    $html .= '</div>

    <div class="comments animated fadeInUp">
    <p class="text-center"></p>
    </div>
    </div>
    </div>';



    return $html;
  }
  add_shortcode( 'depoimentos', 'depoimentos' );


  add_action( 'wpcf7_before_send_mail', 'process_form' );

  function process_form( $cf7 ) {

    if( $cf7->id() == 39 ) {
      global $wpdb;

      $msgs = $cf7->prop('messages');
      $msgs['mail_sent_ok'] = '<h2>Agradecemos o seu contato</h2>';
      $cf7->set_properties(array('messages' => $msgs)); 

    //$submission = WPCF7_Submission::get_instance();


    // get the contact form object
      $cf7 = WPCF7_ContactForm::get_current();

    // do not send the email
    //$cf7->skip_mail = true;



      if ( $submission ) {
        $posted_data = $submission->get_posted_data();
      }

      $existEmail = $wpdb->get_results( $wpdb->prepare("SELECT email FROM `skf_center_contact` where `email` = '".$_POST['your-email']."' ", OBJECT ) );

      if (!count($existEmail)) {
        $wpdb->insert('skf_center_contact',
          array(
            'name' => $_POST['your-name'],
            'email' => $_POST['your-email'],
            'tel' => $_POST['telefone'],
            'name_oficina' => $_POST['oficina'],
            'address_oficina' => $_POST['endereco-oficina'],
            'city_oficina' => $_POST['endereco-cidade'],
            'state_oficina' => $_POST['endereco-estado'],
          ));

      }
      add_filter('wpcf7_skip_mail', 'abort_mail_sending');

    }

  }


  function abort_mail_sending($contact_form){
    return false;
  }


  function addFooterFunction() {
   ?>
   <script type="text/javascript">
    $('.wpcf7-mail-sent-ok').html('<h2>Agradecemos o seu interesse</h2><p>Em breve entraremos em contato, aguarde.</p>');
  </script>

  <?php
}
add_action( 'wp_footer', 'addFooterFunction', 100 );


add_action( 'admin_menu', 'register_my_custom_menu_page' );
function register_my_custom_menu_page() {
  // add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
  add_menu_page( 'Relatórios', 'Relatórios', 'manage_options', 'page_relatorios', 'page_relatorios', 'dashicons-chart-area', 90 );
}

function page_relatorios () {
  require_once "pages-admin/relatorios/relatorio-contacts.php";
}

