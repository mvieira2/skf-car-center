var MAPA = {
	map : "",
	places: null,
	markers : [],
	autocomplete : null,

	init : async function (id) {
		var self = this;

		this.map = new google.maps.Map(document.getElementById(id), {
			zoom: 10,
			center: {lat: -22.916253, lng: -47.08376},
			mapTypeControl: true,
			panControl: true,
			zoomControl: true,
			streetViewControl: true 
		});
		

		this.lojas.map( async function (loja, key2) { // add lojas no mapa
			if (loja.endereco) {
				let end = await self.getGeoCodeByAdress(loja.endereco)
				loja.lat = end.lat();
				loja.lng = end.lng();
			}
			let marker = self.addMarker({
				position: loja,
				map: self.map,
				animation: google.maps.Animation.DROP,
				title: loja.title,
			}, loja.content );

			if (key2==0) {
				google.maps.event.trigger( marker, 'click' ) // abre as infosWindows

			}
		})

		//await self.getMyLocalicacao();

	},
	addMarker : function (marker,content) {
		var self = this; 
		var marker = new google.maps.Marker(marker);
		this.markers.push(marker);

		var infowindow = new google.maps.InfoWindow({
			content: (content ? content : "")
		});

		marker.addListener('click', function() {
			infowindow.open(self.map, marker);
		});

		return marker;
	},

	init_autocomplete : function (id) {
		var self = this;
		this.autocomplete = new google.maps.places.Autocomplete((
			document.getElementById(id)), {
		});

		this.autocomplete.addListener('place_changed', async function (id) {
			{
				var place = self.autocomplete.getPlace();
				if (place.geometry) {

					let enderecoPesquisado = { lat : place.geometry.location.lat(), lng : place.geometry.location.lng()  };
					
					let lojaMaisPerta = await self.lojas.map(async function (loja) {
						let response = await self.searchLojaMaisProximas( enderecoPesquisado, loja);

						if (response) {
							loja.distance = response.rows[0].elements[0].distance.value;

							return loja;
						}
					}).map(async function (loja, currentIndex, lojas) {
						let loja2 = await loja;

						let minDistance = await Promise.all(lojas).then(function (lojas2) {
							return lojas2.map(function (lj) {
								return lj.distance;
							});
						}).then(function (lj2) {
							let min =  Math.min.apply(null, lj2);
							return min;
						})

						if (loja2.distance == minDistance ) {	

							self.map.panTo({lat:loja2.lat, lng:loja2.lng});
							self.map.setZoom(15);


							return loja2;
						}

					});

				} else {
					document.getElementById(id).placeholder = 'Lojas próximas';
				}
			}
		});



	},
	getGeoCodeByAdress : async function (address) { // lat, long

		return new Promise(function(resolve,reject){
			var geocoder = new google.maps.Geocoder();
			geocoder.geocode( { 'address': address}, function(results, status) {
				if (status == 'OK') {
					resolve(results[0].geometry.location)
					return ;
				} else {
					alert('Geocode was not successful for the following reason: ' + status);
				}
			});
		})


	},
	searchLojaMaisProximas : function (origin_LAT_LONG, destino_LAT_LONG, callback) {
		return new Promise( async function (resolve, reject) {

			var service = new google.maps.DistanceMatrixService();
			var origin = new google.maps.LatLng(origin_LAT_LONG["lat"], origin_LAT_LONG["lng"]);
			var destination = new google.maps.LatLng(destino_LAT_LONG["lat"], destino_LAT_LONG["lng"]);

			await service.getDistanceMatrix(
			{
				origins: [origin],
				destinations: [destination],
				travelMode: 'DRIVING',
					//transitOptions: TransitOptions,
					//drivingOptions: DrivingOptions,
					//unitSystem: UnitSystem,
					//avoidHighways: Boolean,
					//avoidTolls: Boolean,
				},function (response) {
					resolve(response);
				});
		})
	},

	getMyLocalicacao : function () {
		var self = this
		return new Promise(function (resolve, reject) {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var pos = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};

					self.map.panTo(pos);
					self.map.setZoom(14);

					resolve(pos);
				}, function() {
					reject({lat: -23.636885, lng: -46.696097});
				});
			}
			else{
				reject({lat: -23.636885, lng: -46.696097});
			}

		})


	},

	lojas : []
}