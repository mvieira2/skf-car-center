<!DOCTYPE html>
<html>
<head>
	<title>SKF - Car Center</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/reset.css?v24">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css?v24">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico">




</head>
<body>

	<header>

		<div class="header-space">

		</div>
		
		<div class="header-fixed">
			<div class="header-top">
				<div class="center">

					<a href="<?php echo home_url(); ?>" class="logo">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png">
					</a>

					<nav class="redes-sociais ">
						<ul class="desktop">
							<li>
								<a target="_blank" href="https://www.facebook.com/SKFBrasil/">
									<i class="fab fa-facebook"></i>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://www.linkedin.com/company/skf/">
									<i class="fab fa-linkedin"></i>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://www.instagram.com/skfgroup">
									<i class="fab fa-instagram"></i>
								</a>
							</li>
							<li>
								<a target="_blank" href="https://www.youtube.com/user/SKFOfficial">
									<i class="fab fa-youtube"></i>
								</a>
							</li>

						</ul>
						<ul class="mobile">
							<li>
								<a href="javascript:;" onclick="document.querySelector('.header-bottom').style.display='block';">
									<i class="fas fa-bars"></i>
								</a>
							</li>
						</ul>
					</nav>

				</div>
			</div>

			<div class="header-bottom" >

				<div class="center">
					<nav class="menu">
						<ul>
							<li class="menu-mobile mobile">
								<a href="javascript:;" onclick="document.querySelector('.header-bottom').style.display='none';">
									<i class="fas fa-times" ></i>
								</a>
							</li>
							<li><a href="<?php echo home_url( 'sobre' );?>">SKF Car Center</a></li>
							<li><a href="javascript:;" onclick="document.getElementById('popup').style.display='block';">Credencie sua oficina</a></li>
							<li><a href="<?php echo home_url( 'encontre-uma-oficina-credenciada' );?>">Encontre uma oficina</a></li>
							<li><a href="<?php echo home_url( 'contato' );?>">Contato</a></li>

						</ul>
					</nav>
				</div>
			</div>

		</div>


	</header>