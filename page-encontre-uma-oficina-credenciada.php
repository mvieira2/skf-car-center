<?php get_header(); ?>

	
<?php 
global $wpdb;
$query = "SELECT post.`post_content` as 'content', ( SELECT `meta_value` FROM `{$wpdb->prefix}postmeta` WHERE  `meta_key` = 'endereco' and `post_id` = post.`ID` limit 1) as 'endereco' FROM `{$wpdb->prefix}posts` as post 
where `post_status` = 'publish' and `post_type` = 'oficinas'";
$all_oficinas = $wpdb->get_results( $query, ARRAY_A   );

?>

<section style="float: left;width: 100%;" class="main">

	<div class="banner ">
		<?php 
		echo do_shortcode('[smartslider3 slider=1]');
		?>
	</div>

	<div class="box1 " data-animate-in="fadeIn">
		<div class="center text-center">
			<p>
				Aqui, você cuida do seu carro com a confiança e qualidade dos  <br>
				produtos da SKF para reposição automotiva.
			</p>
			<br>
			<p>
				Os profissionais das oficinas credenciadas recebem treinamento <br>
				constante e estão qualificados para instalar SKF em seu veículo.
			</p>
		</div>
	</div>

	<div class="mapa">
		<div class="center">
			<h3 class="text-center">Digite um endereço para encontrar oficinas SKF Car Center mais próximas:</h3 class="text-center">
				<div id="locationField">
					<input id="autocomplete" placeholder="Digite aqui um endereço para encontrar oficinas SKF Car Center mais próximas" type="text" />
				</div>
				<div id="map"></div>
			</div>

		</div>

	</section>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4Y-pJ6_H4riHhOtSWb-44W9tG7uRvMuw&libraries=places&callback=initMap"
	async defer></script>

	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/mapa.js"></script>
	<script>
		function initMap() {

			MAPA.lojas = <?php echo json_encode($all_oficinas); ?>;
			MAPA.init("map");
			MAPA.init_autocomplete("autocomplete");
		}

	</script>



	<?php get_footer(); ?>
