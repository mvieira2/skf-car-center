<div class="depoimentos-loop">
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<!-- post thumbnail -->
			<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
				<?php the_post_thumbnail(array(120,120)); // Declare pixel size you need inside the array ?>
			<?php endif; ?>
			<!-- /post thumbnail -->

			<!-- post title -->
			<h2 class="title-page">
				<?php the_title(); ?>
				<br>
				<span><?php echo get_post_meta( get_the_ID(), 'local', true ); ?></span>
			</h2>
			<!-- /post title -->
			<hr>

			<div class="depo">
				<?php the_content(); ?>
			</div>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>
			<h2><?php _e( 'Sorry, nothing to display.', '' ); ?></h2>
		</article>
		<!-- /article -->

	<?php endif; ?>

</div>