
<footer>
	<div class="center">
		<nav>
			<ul>
				<li><a href="<?php echo home_url('sobre'); ?>">Sobre o SKF Car Center</a></li>
				<li><a href="<?php echo home_url('encontre-uma-oficina-credenciada'); ?>">Encontre uma oficina credenciada</a></li>
				<li><a href="<?php echo home_url('beneficios'); ?>">Benefícios para credenciados</a></li>
				<li><a href="<?php echo home_url('depoimentos'); ?>">Depoimentos</a></li>
			</ul>
			<ul>
				<li><a href="mailto:contato@skfcenter.com.br">Contato</a></li>
				<!-- <li><a href="<?php echo home_url('perguntas-frequentes'); ?>">Perguntas frequentes</a></li> -->
				<li><a href="https://www.skf.com/br/footer/terms-and-conditions.html" target="_blank">Termos e condições</a></li>
				<li><a href="https://www.skf.com/br/footer/privacy-policy.html" target="_blank">Política de privacidade</a></li>
			</ul>
		</nav>

		<p class="copyright">
			2019 © SKF do Brasil Todos os direitos reservados
		</p>
	</div>

	<div class="skf-footer">
		<div class="skf-logo"></div>
	</div>
</footer>


<div id="popup" class="popup animated fadeIn">
	<div class="popup-fundo" onclick="document.getElementById('popup').style.display='none';"></div>
	<div class="popup-content">
		<a href="javascript:;" class="popup-close" onclick="document.getElementById('popup').style.display='none';" > <i class="fas fa-times"></i> </a>
		<?php 
		echo do_shortcode('[contact-form-7 title="Formulário de contato 1"]');
		?>

	</div>


</div>

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/includes/fontawesome/css/all.min.css?v24">
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/jquery/jquery-3.3.1.min.js?v24"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/jquery/jquery.mask.min.js?v24"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/config.js?v22"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js?v22"></script>


<script async src="https://www.googletagmanager.com/gtag/js?id=UA-138635940-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-138635940-1');
</script>


<?php wp_footer(); ?>


</body>
</html>